
import httplib, optparse, json, sys

options = ""

RESTPATH = "/rest/modplan/1.0/"

def get_repo():

    h = httplib.HTTPConnection('whiskey.dolby.net', 8085);

    h.request("GET", "/bamboo/rest/modplan/1.0/BAMPLUG-TESTPLANPERFORCE?action=definition")

    response = h.getresponse()

    data = response.read()

    print data


def get_response(method, path):

    h = httplib.HTTPConnection('whiskey.dolby.net', 8085);
    print "PATH: " + path
    h.request(method, path)
    response = h.getresponse()
    data = response.read()
    obj = json.loads(data)
    print "Data: " + data
    return obj


def get_build():
    if len(options.build) == 0:
        print "No build specified"
        sys.exit(1)

    return options.build


def set_enable_build():
    build = get_build()
    if options.set_enable == 'yes':
        value = 'enable'
    elif options.set_enable == 'no':
        value = 'disable'
    else:
        print "Invalid argument for --set-enable"
        sys.exit(1)

    req_str = RESTPATH + "%s/enabled?value=%s" % (build, value)
    get_response("GET", req_str)


def get_enabled():

    build = get_build()
    req_str = RESTPATH + "%s/enabled" % build
    get_response("GET", req_str)


def set_repo_value():
    build = get_build()
    varname = options.set_repo
    value = options.value
    if len(value) == 0:
        print "A value must be specified"
        sys.exit(1)

    path = RESTPATH + "%s/repository?key=%s&value=%s" % (build, varname, value)
    get_response("GET", path)


def get_repo_value():
    build = get_build()
    varname = options.get_repo

    path = RESTPATH + "%s/repository?key=%s" % (build, varname)
    get_response("GET", path)


def get_build_config():
    build = get_build()
    path = RESTPATH + "%s" % build
    get_response("GET", path)


def get_builder_info():
    build = get_build()
    path = RESTPATH + "%s/builder" % build
    get_response("GET", path)
   

def get_builder():   
    build = get_build()
    path = RESTPATH + "%s/builder?key=%s" % (build, options.get_builder)
    get_response("GET", path)


def set_builder():
    build = get_build()
    varname = options.set_builder
    value = options.value
    if len(value) == 0:
        print "A value must be specified"
        sys.exit(1)

    path = RESTPATH + "%s/builder?key=%s&value=%s" % (build, varname, value)
    get_response("GET", path)


def parse_options():

    global options
   
    usage="usage: %prog options\n" 
    parser = optparse.OptionParser(usage=usage)


    parser.add_option("-b", "--build", dest="build", 
                  help="Select build to query / configure",
                  default="")


    parser.add_option("-e", "--set-enable", dest="set_enable", 
                  help="Enable or disable a build with argument values 'yes' or 'no'",
                  default="")

    parser.add_option("--get-enable", dest="get_enable", 
                  help="Query whether a build is enabled or not",
                  default=False, action="store_true")

    parser.add_option("--set-repo", dest="set_repo", 
                  help="Set a repository configuration variable. Specify the variable name " +
                       "with this option and use --value to set the value",
                  default="")

    parser.add_option("--get-repo", dest="get_repo", 
                  help="Fetch a repository configuration variable. Specify the variable name " +
                       "as the option argument",
                  default="")

    parser.add_option("--value", dest="value", 
                  help="Pass an additional value to be used with another option",
                  default="")

    parser.add_option("--get-builder-info", dest="get_builder_info", 
                  help="Get all the information about the 'builder' for the selected build ", 
                  default=False, action="store_true")

    parser.add_option("--get-builder", dest="get_builder", 
                  help="Fetch a builder configuration variable. Specify the variable name " +
                       "as the option argument",
                  default="")

    parser.add_option("--set-builder", dest="set_builder", 
                  help="Set a builder configuration variable. Specify the variable name " +
                       "with this option and use --value to set the value",
                  default="")

    (options, args) = parser.parse_args()

def main():

    parse_options()

    if len(options.set_enable) > 0:
        set_enable_build()
    elif options.get_enable:
        get_enabled()
    elif len(options.set_repo) > 0:
        set_repo_value()
    elif len(options.get_repo) > 0:
        get_repo_value()
    elif options.get_builder_info > 0:
        get_builder_info()
    elif len(options.get_builder) > 0:
        get_builder()
    elif len(options.set_builder) > 0:
        set_builder()
    elif len(options.build) > 0:
        get_build_config()
    else:
        print "No action specified"
        sys.exit(1)
    

main()


