// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "KVList")
@XmlAccessorType(XmlAccessType.FIELD)
public class KVList {

    @XmlElementWrapper(name = "list")
    @XmlElement(name = "pair")
    private List<KVPair> pairs = new ArrayList<KVPair>();

    public KVList() {}


    public void addPair(KVPair pair) {
        pairs.add(pair);
    }
}


